package net.curve84.coreextensions

import junit.framework.TestCase.assertEquals
import org.junit.Test

class ResourceTest {
  @Test
  fun readResourceText() {
    assertEquals("This space intentionally left blank.", this.readResourceText("text_resource.txt"))
  }
}
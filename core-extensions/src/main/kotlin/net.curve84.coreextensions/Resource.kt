package net.curve84.coreextensions

fun Any.readResourceText(resource: String): String {
  return this.javaClass.classLoader.getResource(resource).readText()
}
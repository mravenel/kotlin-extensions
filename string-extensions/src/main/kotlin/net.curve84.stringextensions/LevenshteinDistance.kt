/*
 * Copyright 2016  Michael Ravenel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.curve84.stringextensions

import org.apache.commons.lang3.StringUtils

fun CharSequence.levenshteinDistance(s: CharSequence): Int {
  return StringUtils.getLevenshteinDistance(this, s)
}

fun CharSequence.levenshteinDistance(s: CharSequence, i: Int): Int {
  return StringUtils.getLevenshteinDistance(this, s, i)
}
/*
 * Copyright 2016  Michael Ravenel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.curve84.stringextensions

import junit.framework.TestCase.assertEquals
import org.junit.Test
import java.util.Locale

class FuzzyDistanceTest {
  @Test
  fun testCharSequence() {
    assertEquals(0, "".fuzzyDistance("", Locale.ENGLISH))
    assertEquals(0, "Workshop".fuzzyDistance("b"))
    assertEquals(1, "Room".fuzzyDistance("o"))
    assertEquals(1, "Workshop".fuzzyDistance("w"))
    assertEquals(2, "Workshop".fuzzyDistance("ws"))
    assertEquals(4, "Workshop".fuzzyDistance("wo"))
    assertEquals(3, "Apache Software Foundation".fuzzyDistance("asf"))
  }
}



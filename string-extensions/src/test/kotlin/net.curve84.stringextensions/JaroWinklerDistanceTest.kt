/*
 * Copyright 2016  Michael Ravenel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.curve84.stringextensions

import junit.framework.TestCase.assertEquals
import org.junit.Test

class JaroWinklerDistanceTest {
  @Test
  fun testCharSequence() {
    assertEquals(0.0, "".jaroWinklerDistance(""), 0.1)
    assertEquals(0.0, "".jaroWinklerDistance("a"), 0.1)
    assertEquals(0.0, "aaapppp".jaroWinklerDistance(""), 0.1)
    assertEquals(0.93, "frog".jaroWinklerDistance("fog"), 0.01)
    assertEquals(0.0, "fly".jaroWinklerDistance("ant"), 0.1)
    assertEquals(0.44, "elephant".jaroWinklerDistance("hippo"), 0.01)
    assertEquals(0.44, "hippo".jaroWinklerDistance("elephant"), 0.01)
    assertEquals(0.0, "hippo".jaroWinklerDistance("zzzzzzzz"), 0.1)
    assertEquals(0.88, "hello".jaroWinklerDistance("hallo"), 0.01)
    assertEquals(0.93, "ABC Corporation".jaroWinklerDistance("ABC Corp"), 0.01)
    assertEquals(0.95, "D N H Enterprises Inc".jaroWinklerDistance("D & H Enterprises, Inc."), 0.01)
    assertEquals(0.92, "My Gym Children's Fitness Center".jaroWinklerDistance("My Gym. Childrens Fitness"), 0.01)
    assertEquals(0.88, "PENNSYLVANIA".jaroWinklerDistance("PENNCISYLVNIA"), 0.01)
  }
}

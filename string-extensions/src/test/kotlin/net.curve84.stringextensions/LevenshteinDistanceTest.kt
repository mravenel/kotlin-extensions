/*
 * Copyright 2016  Michael Ravenel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.curve84.stringextensions

import junit.framework.TestCase.assertEquals
import org.junit.Test

class LevenshteinDistanceTest {
  @Test
  fun testCharSequence() {
    assertEquals(0, "".levenshteinDistance(""))
    assertEquals(1, "".levenshteinDistance("a"))
    assertEquals(7, "aaapppp".levenshteinDistance(""))
    assertEquals(1, "frog".levenshteinDistance("fog"))
    assertEquals(3, "fly".levenshteinDistance("ant"))
    assertEquals(7, "elephant".levenshteinDistance("hippo"))
    assertEquals(8, "hippo".levenshteinDistance("zzzzzzzz"))
    assertEquals(1, "hello".levenshteinDistance("hallo"))
  }

  @Test
  fun testThreshold() {
    assertEquals(0, "".levenshteinDistance("", 0))
    assertEquals(7, "aaapppp".levenshteinDistance("", 8))
    assertEquals(7, "aaapppp".levenshteinDistance("", 7))
    assertEquals(7, "elephant".levenshteinDistance("hippo", 7))
    assertEquals(-1, "elephant".levenshteinDistance("hippo", 6))
    assertEquals(7, "hippo".levenshteinDistance("elephant", 7))
    assertEquals(-1, "hippo".levenshteinDistance("elephant", 6))
  }

  @Test(expected = IllegalArgumentException::class)
  fun testNegativeThreshold() {
    "any".levenshteinDistance("any", -1)
  }
}